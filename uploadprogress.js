
/**
 *
 *  @file
 *  File upload progress indication which displays directly in drupal's progress bar system (progress.js / upload.js). 
 *  
 *  TODO:
 * - the apc cached data on a second upload still shows the first one.
 *
 */

// Upload Status.
Drupal.uploadStatus = {  
  // Attach functionality.
  autoAttach: function() {
    $('.uploadprogress-enabled').each(function () {
      Drupal.uploadStatus.addAPC(this.form);
    });
  },
  
  // Attach the functionlity to display upload progress with APC.
  addAPC: function(form) {
    // Create the field and hardcode the type in because of IE not liking JavaScript adding a type later.
    var uuid = ((new Date()).getTime() + "" + Math.floor(Math.random() * 1000000)).substr(0, 18);
    var apc = $('<input type="hidden">').attr({name: Drupal.settings.uploadStatus.name, id: form.name + '-progressbar-key', value: uuid });
    
    // Add submit handler which will display the percentage.
    $(form).submit(Drupal.uploadStatus.submitAPC);
    
    // Make sure that the hidden APC field is always right before and save it for easier access.
    $(form).prepend(apc);
    form.apc = $(form.firstChild);
  },
  
  // Handle the submit with APC.
  submitAPC: function(event) {
    var form = this;
    var getProgress = function() {
      setTimeout(function () {  
        if (form.apc.length) {
	  var uri = Drupal.settings.uploadStatus.callback + form.apc.val();
          $.getJSON(uri, '', function(data, textStatus) {
            // Set the current progress.
            var percent = data.percentage;
	    var message = data.message;
	    if (Drupal.uploadStatus.progress == null) {
	      //create a popup progressbar TBD.
	      var progress = new Drupal.progressBar('uploadprogress');
	      progress.setProgress(percent, message);
	      $(form).after(progress.element);
	      Drupal.uploadStatus.progress = progress;
	      getProgress();
	    }
            else {
              //console.log("percent = %f, message = %s", percent, message);
	      Drupal.uploadStatus.progress.setProgress(percent, message);
	      if (data.status == 1 && (percent >= 0 || (percent == -1 && form.fetch_progress_retries < 3))) {
	        getProgress();	
                form.fetch_progress_retries ++;
              }
            }
          });
        }
      }, Drupal.settings.uploadStatus.interval);
    };

    // Start progress indication.  
    form.fetch_progress_retries = 0;
    getProgress();
  }
}


/**
 *  Handler for the form redirection submission. (taken from progress.js)
 *  We add a line here that shares the progress object back with the uploadStatus object so we can access setting the progress properly.
 */
Drupal.jsUpload.prototype.onsubmit = function () {
  // Insert progressbar and stretch to take the same space.
  this.progress = new Drupal.progressBar('uploadprogress');
  this.progress.setProgress(-1, 'Uploading file');
  
  // Share the progress object with the uploadStatus object so we can set the actual progress.
  Drupal.uploadStatus.progress = this.progress;
  
  var hide = this.hide;
  var el = this.progress.element;
  var offset = $(hide).get(0).offsetHeight;
  $(el).css({
    width: '28em',
    height: offset +'px',
    paddingTop: '10px',
    display: 'none'
  });
  $(hide).css('position', 'absolute');

  $(hide).after(el);
  $(el).fadeIn('slow');
  $(hide).fadeOut('slow');
}


// Global killswitch.
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.uploadStatus.autoAttach);
}
